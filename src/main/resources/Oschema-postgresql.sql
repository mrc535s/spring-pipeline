--------------------------------------------------------
--  File created - Tuesday-April-24-2018   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table UXID_CLEANUP_USER
--------------------------------------------------------

  CREATE TABLE IF NOT EXISTS UXID_CLEANUP_USER 
   (	USERNAME VARCHAR(20), 
	FIRST_NAME VARCHAR(255), 
	LAST_NAME VARCHAR(255), 
	PASSWORD VARCHAR(255), 
	ROLE VARCHAR(20) DEFAULT USER
   );
-- Insert into UXID_CLEANUP_USER (USERNAME,FIRST_NAME,LAST_NAME,PASSWORD,ROLE) values ('KGoff','Kathryn ','Goff','$2a$10$DPN17R1j890iikrSBYmO.ewwbS27sBTWf1lx2kk/Ng8plsMXrItvS','USER');
-- Insert into UXID_CLEANUP_USER (USERNAME,FIRST_NAME,LAST_NAME,PASSWORD,ROLE) values ('dkramme','David','Kramme','$2a$10$Aj38swzNTocvz21eU9h6OuIjZufH7VwAYL8pCabt1POtZFAep3WZW','ADMIN');
-- Insert into UXID_CLEANUP_USER (USERNAME,FIRST_NAME,LAST_NAME,PASSWORD,ROLE) values ('dledbetter','Denise','Ledbetter','$2a$10$Z2W9UgVrNfkaqWjjOG1F4.9lRa5Us7Fq6ZCzsrYTPQEVxMw8.dg8K','ADMIN');
-- Insert into UXID_CLEANUP_USER (USERNAME,FIRST_NAME,LAST_NAME,PASSWORD,ROLE) values ('mrc535s','Mike','Carney','$2a$10$9LF5/TZM4buxJ1r7ZDrcv.c9dcgulrPpXBIe32raMYcMnmv7VwaKK','ADMIN');
-- commit;
--------------------------------------------------------
--  DDL for Index UXID_CLEANUP_USER_PK
--------------------------------------------------------

--   CREATE UNIQUE INDEX UXID_CLEANUP_USER_PK ON UXID_CLEANUP_USER (USERNAME);
-- --------------------------------------------------------
-- --  Constraints for Table UXID_CLEANUP_USER
-- --------------------------------------------------------

--   ALTER TABLE UXID_CLEANUP_USER ADD CONSTRAINT UXID_CLEANUP_USER_PK PRIMARY KEY (USERNAME);
