import { EventEmitter } from 'events';
import {map} from 'ramda';

import AppDispatcher from '../dispatcher/AppDispatcher';
import ActionTypes from '../constants/AppConstants';


const CHANGE = 'change';
const DEFAULT_PAGE = 1;
const DEFAULT_NUM_PER_PAGE = 10;
const TOTAL_PAGES = 0;

class CleanupStore extends EventEmitter {

    constructor() {
        super();
        this.selected = [];
        this.tableData = [];
        this.messages = {
            type: '',
            message: ''
        };
        this.filters = ActionTypes.INIT_FILTERS;
        this.dispatchToken = AppDispatcher.register(this.dispatcherCallback.bind(this))
        this.loading = false;
        this.pageNumber = DEFAULT_PAGE;
        this.numPerPage = DEFAULT_NUM_PER_PAGE;
        this.totalPages = TOTAL_PAGES;
        this.user = null;
        this.initialized = false;
    }

    emitChange(eventName) {
        this.emit(eventName);
    }

    getSelected() {
        return this.selected;
    }

    getTableData() {
      return this.tableData;
    }

    getFilters() {
        return this.filters;
    }

    getLoading() {
        return this.loading;
    }

    getMessages() {
        return this.messages;
    }

    getPageNum() {
        return this.pageNumber;
    }

    getNumPerPage() {
        return this.numPerPage;
    }

    getTotalPages() {
        return this.totalPages;
    }

    getUser() {
        return this.user;
    }

    getInitialized() {
        return this.initialized;
    }

    updateFilters(filter) {
        this.filters = {...this.filters, ...filter};
    }

    filterData() {
      this.setLoading(true);
    }

    clearFilters() {
        this.tableData = [];
        this.selected = [];
        this.filters = map(() => '', this.filters);
    }

    changePage(pageNumber) {
        this.pageNumber = pageNumber;
        this.selected = [];
    }

    updateNumPerPage(numPerPage) {
        this.numPerPage = numPerPage;
    }

    updateTotalPages(total) {
        this.totalPages = total;
    }

    updateMessage(messageData) {
        this.messages = {...messageData};
    }

    clearMessages() {
        this.messages = {
            type: '',
            message: ''
        }
    }

    tableDataLoaded(data) {
        this.updateTotalPages(data.totalPages);
        this.tableData = data.content;
        
        this.setLoading(false);
    }
    submitUpdate() {
        this.selected = [];
        this.setLoading(true);
    }

    submitUpdateComplete() {
        this.setLoading(false);
    }

    setLoading(loading) {
        this.loading = loading;
    }

    setUser(user) {
        this.user = user;
    }

    setInitialized(init) {
        this.initialized = init;
    }

    updateSelected(data) {
      if (this.selected && this.selected.includes(data.value)) {  
        this.selected = this.selected.filter((item) => item !== data.value)
      } else {
        this.selected.push(data.value);
      }
    }

    addChangeListener(callback) {
        this.on(CHANGE, callback);
    }

    removeChangeListener(callback) {
        this.removeListener(CHANGE, callback);
    }

    dispatcherCallback(action) {
        switch (action.type) {
            case ActionTypes.INITIALIZE_COMPLETE:
                if (action && action.data) {
                    this.setUser(action.data.user);
                    this.setInitialized(true);
                    this.emitChange(CHANGE);
                }
                break;
            case ActionTypes.UPDATE_SELECTED:
                this.updateSelected(action.data);
                this.emitChange(CHANGE);
                break;
            case ActionTypes.FILTER_DATA:
                this.filterData(action.data);
                this.emitChange(CHANGE);
              break;
            case ActionTypes.CLEAR_FILTERS:
                this.clearFilters(action.data);
                this.emitChange(CHANGE);
                break;
            case ActionTypes.SUBMIT_UPDATE:
                this.submitUpdate(action.data);
                this.emitChange(CHANGE);
                break;
            case ActionTypes.TABLE_DATA_LOADED:
                this.tableDataLoaded(action.data);
                this.emitChange(CHANGE);
                break;
            case ActionTypes.UPDATE_FILTERS:
                this.updateFilters(action.data);
                this.emitChange(CHANGE);
                break;
            case ActionTypes.SUBMIT_UPDATE_COMPLETE:
                this.submitUpdateComplete();
                this.emitChange(CHANGE);
                break;
            case ActionTypes.UPDATE_MESSAGES:
                this.updateMessage(action.data);
                this.emitChange(CHANGE);
                break;
            case ActionTypes.CLEAR_MESSAGES:
                this.clearMessages();
                this.emitChange();
                break;
            case ActionTypes.PAGE_CHANGE:
                this.changePage(action.data);
                this.emitChange(CHANGE);
                break;
            case ActionTypes.NUM_PER_PAGE_CHANGE:
                this.updateNumPerPage(action.data);
                this.emitChange(CHANGE);
                break;
            case ActionTypes.SETLOADING:
                this.setLoading(action.data);
                this.emitChange(CHANGE);
                break;
            default: 
                // no op
        }

        return true;
    }
}

export default new CleanupStore();