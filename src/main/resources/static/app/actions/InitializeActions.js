import AppDispatcher from '../dispatcher/AppDispatcher';
import ActionTypes from '../constants/AppConstants';

const InitialzeActions = {
    initApp() {
        this.getActiveUser();
    },
    getActiveUser() {
        fetch('/security/activeUser', {
            credentials: 'include'})
            .then((response) => response.text())
            .catch(error => this.ajaxError(error))
            .then((responseText) => {
                this.getActiveUserDetails(responseText);
            });
    },
    getActiveUserDetailsSuccess(response) {
      console.log(response);
        AppDispatcher.dispatch({
          type: ActionTypes.INITIALIZE_COMPLETE,
          data: {
            user: response
          },
        });
    },
    ajaxError(error) {
      console.error(error);
    },
    getActiveUserDetails(userName) {
        const URL = `/security/activeUserDetails?userName=${userName}`
        fetch(URL, {
            credentials: 'include'})
            .then((response) => response.json())
            .catch(error => this.ajaxError(error))
            .then((responseJSON) => {
                this.getActiveUserDetailsSuccess(responseJSON);
            });
    }
  
};

export default InitialzeActions;