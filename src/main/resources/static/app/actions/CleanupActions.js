// require('es6-promise').polyfill();
import 'whatwg-fetch';

import AppDispatcher from '../dispatcher/AppDispatcher';
import ActionTypes from '../constants/AppConstants';
import CleanupStore from '../stores/CleanupStore';
import Utils from '../utils/Utils';
import UrlConstants from '../constants/UrlConstants';

const CleanupActions = {

  updateSelected(selectedData) {
    AppDispatcher.dispatch({
      type: ActionTypes.UPDATE_SELECTED,
      data: selectedData,
    });
  },
  updateFilters(filter) {
    AppDispatcher.dispatch({
      type: ActionTypes.UPDATE_FILTERS,
      data: filter,
    });
  },
  filterData(filters) {
    AppDispatcher.dispatch({
      type: ActionTypes.FILTER_DATA,
      data: filters,
    });
    const data = {
      ...CleanupStore.getFilters(),
      pageNumber: CleanupStore.getPageNum(),
      pageSize: CleanupStore.getNumPerPage()
    };

    fetch(UrlConstants.SOURCEDATAURL, {
      method: 'POST',
      body: JSON.stringify(data), 
      headers: new Headers({
        'Content-Type': 'application/json'
      }) }).then((response) => 
      response.json())
      .catch(error => this.ajaxError(error))
      .then((responseJson) => {
      this.tableDataLoaded(responseJson);
    }).catch(error => this.ajaxError(error));
  },
  clearFilters(filters) {
    AppDispatcher.dispatch({
      type: ActionTypes.CLEAR_FILTERS,
      data: filters,
    });
  },
  submitUpdate(formData, tableData, selected) {
    AppDispatcher.dispatch({
      type: ActionTypes.SUBMIT_UPDATE,
    });
    const data = tableData
      .filter(row => selected.includes(Utils.getRowId(Utils.flatten(row))))
      .map(row => {
    	  row.bucket = row.currentBucket;
    	  row.approverName = `${CleanupStore.getUser().firstName} ${CleanupStore.getUser().lastName}`;
    	  return {...Utils.flatten(row), ...formData}
      });
      
    fetch(UrlConstants.AUDITURL, {
      method: 'POST',
      body: JSON.stringify(data), 
      headers: new Headers({
        'Content-Type': 'application/json'
      }) })
      .then((response) => this.submitUpdateCompleteSuccess(response))
      .catch(error => this.ajaxError(error));
  },
  submitUpdateCompleteSuccess() {
    const successMessage = 'Audit Rows Successfully Updated';
    const SUCCESS_TYPE = 'success';
    this.updateMessages(successMessage, SUCCESS_TYPE);
    AppDispatcher.dispatch({
      type: ActionTypes.SUBMIT_UPDATE_COMPLETE,
    });
    this.filterData();
  },
  ajaxError(error) {
    const ERROR_TYPE = 'error';
    this.setLoading(false);
    this.updateMessages(`There was an error retreiving data: ${error}`, ERROR_TYPE)
  },
  tableDataLoaded(data) {
    AppDispatcher.dispatch({
      type: ActionTypes.TABLE_DATA_LOADED,
      data
    });

    if (data && data.content && data.content.length <= 0) {
      const warningMessage = 'No Data Found for Current Filter Set';
      const WARNING_TYPE = 'warning';
      this.updateMessages(warningMessage, WARNING_TYPE);
    }
  },
  updateMessages(message, type) {
    AppDispatcher.dispatch({
      type: ActionTypes.UPDATE_MESSAGES,
      data: {
        message,
        type
      }
    });
  },
  pageChange(pageNum) {
    AppDispatcher.dispatch({
      type: ActionTypes.PAGE_CHANGE,
      data: pageNum
    });
    this.filterData();
  },
  updateNumPerPage(numPerPage) {
    AppDispatcher.dispatch({
      type: ActionTypes.NUM_PER_PAGE_CHANGE,
      data: numPerPage
    });

    this.pageChange(ActionTypes.DEFAULT_PAGE);
  },
  setLoading(loading) {
    AppDispatcher.dispatch({
      type: ActionTypes.SETLOADING,
      data: loading
    })
  }

};

export default CleanupActions;
