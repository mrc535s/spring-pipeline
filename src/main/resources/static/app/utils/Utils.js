const ADMIN = 'ADMIN';
const utils = {
    /**
 * PRIVATE
 * Flatten a deep object into a one level object with it’s path as key
 *
 * @param  {object} object - The object to be flattened
 *
 * @return {object}        - The resulting flat object
 */
    flatten: object => 
        Object.assign(
            {}, 
            ...function _flatten(o) { 
            return [].concat(...Object.keys(o)
                .map(k => 
                typeof o[k] === 'object' && o[k] !== null ?
                    _flatten(o[k]) : 
                    ({[k]: o[k]})
                )
            );
        }(object)
      ),
    getRowId: row =>
        row.id + '-' + row.idType + '-' + row.sourceSystem + '-' + row.sourceRecordKey,
    isAdmin: user => user && user.role === ADMIN
};

export default utils