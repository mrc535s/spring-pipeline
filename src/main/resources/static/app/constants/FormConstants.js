const FormConstants = {
    actionAgreedOptions:[
        { key: 'none', text: 'Select an Action Agreed', value: ''},
        { key: 'workerid', text: 'Updated NetIQ WorkerID#', value: 'Updated NetIQ WorkerID#' },
        { key: 'addresearch', text: 'Additional Research needed', value: 'Additional Research needed' },
        { key: 'xtog', text: 'Moved from XID to GID', value: 'Moved from XID to GID' },
        { key: 'nodel', text: 'Do not Delete', value: 'Do not Delete' },
        { key: 'makeavail', text: 'Make Available', value: 'Make Available' },
    
      ], 
      uxidEntityTypeOptions: [
        { key: 'all', text: 'All UXID Entity Types', value: ''},
        { key: 'x', text: 'XID', value: 'X' },
        { key: 'p', text: 'PID', value: 'P' },
        { key: 'G', text: 'Generic', value: 'G' }
      ],
      currentBucketOptions: [
        { key: 'all', text: 'All Current Buckets', value: ''},
        { key: 'termed', text: 'Termed', value: 'Termed' },
        { key: 'activeempno', text: 'Active Worker, No Activity', value: 'Active Worker, No Activity' },
        { key: 'unknownno', text: 'Unknown, No Activity', value: 'Unknown, No Activity' },
        { key: 'unknown', text: 'Unknown, With Activity', value: 'Unknown, With Activity' },
        { key: 'gennoact', text: 'Generic, No Activity', value: 'Generic, No Activity' },
        { key: 'dupe', text: 'Dupe', value: 'Dupe' },
        { key: 'multi', text: 'Multi', value: 'Multi' }
      ],
      maOptions: [
        { key: 'all', text: 'All Management Areas', value: ''},
        { key: 'Central States', text: 'Central States', value: 'Central States' },
        { key: 'Central Texas', text: 'Central Texas', value: 'Central Texas' },
        { key: 'Eastern NC', text: 'Eastern NC', value: 'Eastern NC'},
        { key: 'Eastern NY', text: 'Eastern NY', value: 'Eastern NY' },
        { key: 'Greenville, SC / Georgia', text: 'Greenville, SC / Georgia', value: 'Greenville, SC / Georgia' },
        { key: 'Kansas City / Lincoln', text: 'Kansas City / Lincoln', value: 'Kansas City / Lincoln' },
        { key: 'Michigan', text: 'Michigan', value: 'Michigan'},
        { key: 'Minnesota', text: 'Minnesota', value: 'Minnesota' },
        { key: 'Mountain States', text: 'Mountain States', value: 'Mountain States' },
        { key: 'North Texas', text: 'North Texas', value: 'North Texas'},
        { key: 'Pacific Northwest', text: 'Pacific Northwest', value: 'Pacific Northwest' },
        { key: 'So Cal Central', text: 'So Cal Central', value: 'So Cal Central' },
        { key: 'So Cal North', text: 'So Cal North', value: 'So Cal North' },
        { key: 'So Cal South', text: 'So Cal South', value: 'So Cal South'},
        { key: 'Southern New England', text: 'Southern New England', value: 'Southern New England' },
        { key: 'TN / LA / AL', text: 'TN / LA / AL', value: 'TN / LA / AL' },
        { key: 'Wisconsin', text: 'Wisconsin', value: 'Wisconsin' }  
      ],
      dateRangeOptions: [
        { key: 'all', text: 'All Dates', value: ''},
        { key: 'lt90', text: 'Newer than 90 days', value: 'Newer than 90 days' },
        { key: '180to90', text: 'Between 180 and 90 days', value: 'Between 180 and 90 days' },
        { key: '420to180', text: 'Between 420 and 180 days', value: 'Between 420 and 180 days' },
        { key: 'grt420', text: 'Older than 420 days', value: 'Older than 420 days' },
        { key: 'notfound', text: 'Date not found', value: 'Date not found' },
      ],
      sourceSystemOptions:  [
        { key: 'all', text: 'All Source Systems', value: ''},
        { key: 'bhn', text: 'BHN', value: 'BHN' },
        { key: 'cht', text: 'CHT', value: 'CHT' },
        { key: 'twc', text: 'TWC', value: 'TWC' },
      ],
      idTypeOptions: [
        { key: 'all', text: 'All Id Types', value: ''},
        { key: 'salesid', text: 'SALESID', value: 'SALESID' },
        { key: 'techid', text: 'TECHID', value: 'TECHID' },
        { key: 'opid', text: 'OPID', value: 'OPID' },
      ]

    };
export default FormConstants;