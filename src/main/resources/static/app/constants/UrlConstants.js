const UrlConstants = {
    SOURCEDATAURL: '/source/getSourceData',
    AUDITURL: '/audit'
};

export default UrlConstants;