import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { ToastContainer, toast } from 'react-toastify';
import CleanupStore from '../stores/CleanupStore';

class Toaster extends Component {

  static propTypes = {
    message: PropTypes.string.isRequired,
    type: PropTypes.string.isRequired
  }

  toastId = null

  clearToasts = () => {
    CleanupStore.clearMessages();
    this.toastId = null;
  }
  showMessage = () => {
    if (!this.toastId) {
      this.toastId = toast(this.props.message, {
        type: toast.TYPE[this.props.type.toUpperCase()],
        onClose: () => this.clearToasts()
      });
      
    }
  }

  render() {
    return (
      <div>
        {<ToastContainer />}
        {(this.props.type !== '') ?
        this.showMessage() : null}
      </div>
    );
  }
}

export default Toaster;
