import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Dropdown, Pagination } from 'semantic-ui-react';
import CleanupActions from '../actions/CleanupActions';

class TablePagination extends Component {

  static propTypes = {
    activePage: PropTypes.number.isRequired,
    numPerPage: PropTypes.number.isRequired,
    totalPages: PropTypes.number.isRequired
  };

  state = {
    boundaryRange: 1,
    siblingRange: 1,
    showEllipsis: true,
    showFirstAndLastNav: false,
    showPreviousAndNextNav: true,
  };

  handlePaginationChange = (e, { activePage }) => {
    CleanupActions.pageChange(activePage);
  };

  handleNumPerPageChange = (e, {value}) => {
    const numPerPage = value;
    CleanupActions.updateNumPerPage(numPerPage)
  };

  render() {
    const {
      boundaryRange,
      siblingRange,
      showEllipsis,
      showFirstAndLastNav,
      showPreviousAndNextNav,
    } = this.state;
    const {
      activePage,
      numPerPage,
      totalPages
    } = this.props;
    const numPerPageOptions = [
      {key: '10', text: '10', value: 10},
      {key: '25', text: '25', value: 25},
      {key: '50', text: '50', value: 50},
      {key: '100', text: '100', value: 100}
    ];
    return (
      <div>
      <Dropdown onChange={this.handleNumPerPageChange} name='numPerPage' value={numPerPage} upward compact selection 
        options={numPerPageOptions} style={{fontSize: '14px', margin: '0 10px', minHeight: '40px'}} />
      <Pagination   
          activePage={activePage}
          boundaryRange={boundaryRange}
          onPageChange={this.handlePaginationChange}
          totalPages={totalPages}
          siblingRange={siblingRange}
          // Heads up! All items are powered by shorthands, if you want to hide one of them, just pass `null` as value
          ellipsisItem={showEllipsis ? undefined : null}
          firstItem={showFirstAndLastNav ? undefined : null}
          lastItem={showFirstAndLastNav ? undefined : null}
          prevItem={showPreviousAndNextNav ? undefined : null}
          nextItem={showPreviousAndNextNav ? undefined : null}
      /> </div>
    );
  }
}

export default TablePagination;
