import React, { Component } from 'react';
import { Icon, Table, Checkbox, Grid, Header, Popup} from 'semantic-ui-react'
import PropTypes from 'prop-types';
import {pick, splitAt, map, compose} from 'ramda';
import CleanupActions  from '../actions/CleanupActions';
import UpdateTableForm from './UpdateTableForm';
import TablePagination from './TablePagination';
import CommentModal from './CommentModal';
import Utils from '../utils/Utils';

const excludes = ['workOrderDate', 'workOrderNumber', 'equipmentDate', 'equipmentNumber', 'jobDate', 'jobNumber', 'sourceSystem', 'idType',
  'workAssureLoginId', 'workAssureFirstName', 'workAssureLastName', 'workAssureRole', 
  'netIqEntityAccount', 'netIqFirstName', 'netIqLastName', 'netIqTermedDateRange',
  'netIqCompany', 'netIqSupervisorId', 'netIqRegion', 'netIqTermedDate',
  'netIqHireDate', 'netIqHireDateRange', 'uxidEntity', 'uxidEntityAccount', 
  'approverName', 'approvedDate', 'actionTaken', 'actionTakenNotes',
  'actionTakenDate', 'bucket'
];

const mainRow = ['currentBucket', 'workOrderDateRange', 'equipmentDateRange', 'jobDateRange', 'id', 'siteId', 'netIqWorkerId',
  'firstName', 'lastName', 'managementArea', 'netIqStatus', 'netIqDepartment', 'netIqJobType', 'teamName', 'actionAgreed',
 'approverComments', 'approvedDate', 'notes'];

class CleanupTable extends Component {
  state = {
    toggleDetails: []
  };

  getSubRow = (row, idx) => {
    const SPLIT_LINES = 14;
    return pick(splitAt(SPLIT_LINES, excludes)[idx], row)
  };

  getSubRows = (row) => {
    const rowIdx = [0, 1];
    return rowIdx.map((i) =>
    <Grid.Row>
    { 
      Object.keys(this.getSubRow(row, i)).map(header =>
        <Grid.Column>
        <div>
          <Header sub>{this.formatHeaderText(header)}</Header>
          <span>{row[header]}</span>
        </div>
        </Grid.Column>
    )}
    </Grid.Row>)
  };

  getIconName = (header) => {
    switch (header) {
      case 'approverComments':
        return 'comments';
        break;
      case 'notes':
        return 'sticky note';
        break;
      default:
        return 'comments';
    }
  };
    
  getFormattedRow = (row) => Utils.flatten(row);

  getHeaderRow = () => 
    mainRow.map(header =>
      <Table.HeaderCell>{this.formatHeaderText(header)}</Table.HeaderCell>
    );

  getHeaderRows = () =>
      <Table.Row key='headerRow'>
      {/* Save for release 2 <Table.HeaderCell><Checkbox value='all' onChange={this.rowChecked}/></Table.HeaderCell> */}
      <Table.HeaderCell>&nbsp;</Table.HeaderCell> 
     {
        this.getHeaderRow()
     }
      <Table.HeaderCell>&nbsp;</Table.HeaderCell>
      </Table.Row>;
  
  getRow = (row) => {
    const rowId = Utils.getRowId(row);
    return <Table.Body>
    <Table.Row key={rowId} active={this.isRowSelected(rowId)}>
  <Table.Cell collapsing><Checkbox value={rowId} disabled={this.disableCheckbox(row, this.props.user)} onChange={this.rowChecked} checked={this.isRowSelected(rowId)}/></Table.Cell>
    {mainRow.map(header =>
    	header!=='approverComments' && header!=='notes' ?
    <Table.Cell>{row[header]}</Table.Cell> : 
    <Table.Cell collapsing textAlign='center'>
      {row[header] && row[header] !== null && header==='approverComments' ?
      <Popup trigger={<Icon link name={this.getIconName(header)} />} content={row[header]}/> : header==='notes' ?
      <CommentModal data={this.props.data} rowId={rowId} notes={row[header]} /> : null
     
      }
    </Table.Cell>
  )}
  <Table.Cell collapsing>
  { (this.state.toggleDetails.includes(rowId)) ?
    <Icon name='chevron up' link onClick={()=>this.toggleDetails(rowId)}/> :
    <Icon name='chevron down' link onClick={()=>this.toggleDetails(rowId)}/>}
  </Table.Cell>
  </Table.Row>
  { (this.state.toggleDetails.includes(rowId)) ?
  <Table.Row key={`sub-{rowId}`} active={this.isRowSelected(rowId)}>
  <Table.Cell style={{borderBottom: '1px solid rgba(34,36,38,.1)'}} colSpan={this.getHeaderCount()}>
  <Grid columns='equal' style={{ padding: '40px 25px' }}>
  {this.getSubRows(row)}
  </Grid>
  </Table.Cell>

  </Table.Row> : null}
  </Table.Body>
  };

  getRows = () => 
    compose(
      map(this.getRow),
      map(this.getFormattedRow)
    )(this.props.data);

  getHeaderCount = () => {
    if (this.props.data.length >= 1) {
      return Object.keys(this.props.data[0]).length + 1;
    }
    return 0;
  };

  disableCheckbox = (row, user) => (row.actionAgreed && !Utils.isAdmin(user));

  toggleDetails = (id) => {
    if (this.state.toggleDetails && this.state.toggleDetails.includes(id)) {
      this.setState({
        toggleDetails: this.state.toggleDetails.filter((rowId) => rowId !== id)
      })  
    } else {
      this.setState({
        toggleDetails: [...this.state.toggleDetails, id]
      })
    }
  };


  isRowSelected = (id) => 
    this.props.selected.includes(id);
  
  
  formatHeaderText = (text) => {
    // TODO: Make Functional - remove slice
    const textFormatted = text.charAt(0).toUpperCase() + text.slice(1);
    return textFormatted.split(/(?=[A-Z])/).join(' ');
  };

  rowChecked = (evt, data) => {
    CleanupActions.updateSelected(data);
  };

  render() {
    return (
      <Table compact size='small' celled style={{fontSize: '.75em'}}>
      <Table.Header>
      {(this.props.selected.length) ? 
      <UpdateTableForm selected={this.props.selected} data={this.props.data} colSpan={this.getHeaderCount()}/> : null }
      {this.getHeaderRows()}
    </Table.Header>
      {this.getRows()}
    <Table.Footer>
      <Table.Row>
        <Table.HeaderCell colSpan={this.getHeaderCount()} textAlign='right'>
        <TablePagination totalPages={this.props.totalPages} activePage={this.props.currentPage} numPerPage={this.props.numPerPage}/>
        </Table.HeaderCell>
      </Table.Row>
    </Table.Footer>
  </Table>
    );
  }
}

CleanupTable.propTypes = {
  data: PropTypes.arrayOf(PropTypes.object).isRequired,
  selected: PropTypes.arrayOf(PropTypes.string).isRequired,
  currentPage: PropTypes.number.isRequired,
  numPerPage: PropTypes.number.isRequired,
  totalPages: PropTypes.number.isRequired,
  user: PropTypes.objectOf(PropTypes.string).isRequired
};

export default CleanupTable;
