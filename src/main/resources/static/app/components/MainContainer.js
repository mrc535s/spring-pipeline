import React, { Component } from 'react';
import { Header, Container, Segment} from 'semantic-ui-react';
import FiltersForm from './FiltersForm';
import Toaster from './Toaster';
import CleanupTable from './CleanupTable';
import CleanupStore from '../stores/CleanupStore';
import Navigation from "./Navigation";

class MainContainer extends Component {

  state = {
    data: CleanupStore.getTableData(),
    selected: CleanupStore.getSelected(),
    messages: CleanupStore.getMessages(),
    loading: CleanupStore.getLoading(),
    filters: CleanupStore.getFilters(),
    currentPage: CleanupStore.getPageNum(),
    numPerPage: CleanupStore.getNumPerPage(),
    totalPages: CleanupStore.getTotalPages(),
    user: CleanupStore.getUser(),
  }

  componentWillMount = () => {
    CleanupStore.addChangeListener(this.onChange);
  }

  componentWillUnmount = () => {
    CleanupStore.removeChangeListener(this.onChange);
  }

  onChange = () => {
    this.setState({
      data: CleanupStore.getTableData(),
      selected: CleanupStore.getSelected(),
      loading: CleanupStore.getLoading(),
      filters: CleanupStore.getFilters(),
      messages: CleanupStore.getMessages(),
      currentPage: CleanupStore.getPageNum(),
      numPerPage: CleanupStore.getNumPerPage(),
      totalPages: CleanupStore.getTotalPages(),
      user: CleanupStore.getUser()
    })
  };

  getContainerText = () => {
    if(!this.state.loading) {
      return 'Select filter(s) above to start data cleanup...';
    }
    return 'Loading...';
  };

  render() {
    const {
      data,
      selected,
      loading,
      filters,
      messages,
      currentPage,
      numPerPage,
      totalPages,
      user
    } = this.state;
    return (
    <div>
      <Navigation user={user}/>
      <Toaster message={messages.message} type={messages.type}/>
      <Container style={{ marginTop: '3em', padding: '10px'}} fluid>
        <Segment>

          <FiltersForm filters={filters} />
        </Segment>
      </Container>
      <Container style={{padding: '10px' }} fluid>

        {data.length > 0 && !loading ? <CleanupTable user={user} totalPages={totalPages} data={data} numPerPage={numPerPage} selected={selected} currentPage={currentPage}/> :
          <Segment textAlign='center' loading={loading} padded='very'>
          <Header as='h3'>{this.getContainerText()} </Header>
        </Segment> }
      </Container>
    </div>
    );
  }
}

export default MainContainer;
