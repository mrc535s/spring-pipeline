import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {Table, Form, Label, Message} from 'semantic-ui-react';

import CleanupActions from '../actions/CleanupActions';
import FormConstants from '../constants/FormConstants';
import Utils from '../utils/Utils';

class UpdateTableForm extends Component {

  static propTypes = {
    colSpan: PropTypes.number.isRequired,
    data: PropTypes.arrayOf(PropTypes.object).isRequired,
    selected: PropTypes.arrayOf(PropTypes.string).isRequired
  };

  state = {
    updateActionForm: {
      approverComments: '',
      actionAgreed: '',
      netIqWorkerId: ''
    },
    valid: {
      actionAgreed: true,
      actionAgreedList: []
    }
  };

  submitUpdate = () => {
    if (this.validateForm()) {
      if (this.state.updateActionForm.approverComments.length > 0) {
        CleanupActions.submitUpdate(this.state.updateActionForm, this.props.data, this.props.selected);
      } else {
        const {approverComments, ...noCommentsForm} = this.state.updateActionForm; // Removes the comments from the object without mutating
        CleanupActions.submitUpdate(noCommentsForm, this.props.data, this.props.selected);
      }
      this.clearFormValues();
    }
  };

  clearFormValues = () => {
    this.setState(() => ({
      updateActionForm: {approverComments: ''}
    }));
  };

  validateForm = () => {
    const {actionAgreed, netIqWorkerId} = {...this.state.updateActionForm};
    let messages = [];
    let valid = true;
    this.setState({
      valid: {
        actionAgreed: true,
        actionAgreedList: [],
        netIqWorkerId: true
      }
    });

    if (actionAgreed === ''){
      this.setState((prevState) => ({
        valid: {...prevState.valid, actionAgreed: false}
      }));
      valid = false;
    }

     /** NetIQWorkerId Validation **/
      if (actionAgreed === 'Updated NetIQ WorkerID#') {
         if(this.props.selected.length > 1) {
           messages.push('Only one row can update NetIQ Worker ID at a time');
             this.setState((prevState) => ({
               valid: {...prevState.valid,
                 actionAgreedList: messages
               },
             }));
             valid = false;
         }
        if(this.props.data.filter(row => this.props.selected.includes(Utils.getRowId(Utils.flatten(row))) &&
          (row.netIqWorkerId && row.netIqWorkerId.length > 0)).length > 0) {
          messages.push('Only a row with an empty NetIQWorkerId can be updated');
          this.setState((prevState) => ({
            valid: {...prevState.valid,
              actionAgreedList: messages
            },
          }));
          valid = false;
        }
        if (netIqWorkerId === ''){
          this.setState((prevState) => ({
            valid: {...prevState.valid, netIqWorkerId: false}
          }));
          valid = false;
        }
      }
    return valid;
  };
  
  handleChange = (e, { name, value }) =>  {
	  this.setState({updateActionForm: { ...this.state.updateActionForm, [name]: value }}, () => this.validateForm());
  };

  render() {
    const {actionAgreed, approverComments, netIqWorkerId} = {...this.state.updateActionForm};
    return (
      <Table.Row key='updateRows' >
        <Table.HeaderCell  colSpan={this.props.colSpan}>
        <Form size='small' error>
            <Form.Group>
                <Form.Field width={3}>
                  <Form.Select  error={!this.state.valid.actionAgreed}  name='actionAgreed' value={actionAgreed} options={FormConstants.actionAgreedOptions} placeholder='Select an Action' onChange={this.handleChange}/>
                    {!this.state.valid.actionAgreed ? <Label basic color='red' pointing>Please select a value</Label> : null}
                  {this.state.valid.actionAgreedList.length > 0 ? <Message
                    error
                    list={this.state.valid.actionAgreedList}
                  /> : null}
                </Form.Field>
              { actionAgreed === 'Updated NetIQ WorkerID#' ? <Form.Field width={3}>
                <Form.Input value={netIqWorkerId}  name='netIqWorkerId' rows={1} placeholder='Enter a NetIqWorker Id' onChange={this.handleChange}/>
                {!this.state.valid.netIqWorkerId ? <Label basic color='red' pointing>Please enter a value</Label> : null}
              </Form.Field> : null}
                <Form.Field width={6}>
                    <Form.Input value={approverComments}  name='approverComments' rows={1} maxLength='150' placeholder='Add Comments' onChange={this.handleChange}/>
                </Form.Field>
                <Form.Button size='small' onClick={this.submitUpdate} primary>Update</Form.Button>
            </Form.Group>
        </Form>
        </Table.HeaderCell>
      </Table.Row>
    );
  }
}

export default UpdateTableForm;
