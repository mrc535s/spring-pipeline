import React, { Component } from 'react';
import PropTypes from 'prop-types';
import  {Modal, Button, TextArea, Icon, Label, Form, Popup} from 'semantic-ui-react';

import CleanupActions from '../actions/CleanupActions';
const initState = {
	    commentsForm: {
	        notes: ''
	      },
	      valid: {
	          message: true
	        },
	      modalOpen: false,
	    };

class CommentModal extends Component {
	
  static propTypes = {
    data: PropTypes.arrayOf(PropTypes.object).isRequired,
    rowId: PropTypes.string.isRequired,
    notes: PropTypes.string
  };

  notesExist = () => {
   return (this.props.notes && this.props.notes.length > 0);
  };

  getInitState = () => {
    if (this.notesExist()) {
      return {...initState, commentsForm: {notes: this.props.notes}}
    }
    return initState;
  };

  state = this.getInitState();

  getIconName = () => {
    return this.notesExist() ? 'sticky note' : 'add';
  };
  
  addComment = (rowId) => {

	  if (this.validateForm()) {
		  CleanupActions.submitUpdate(this.state.commentsForm, this.props.data, [rowId]);
		  this.handleClose();
	  	}
  };

  validateForm = () => {
	    if (this.state.commentsForm.notes.length == 0) {
	      this.setState({
	        valid: {
	          message: false
	        }
	      });
	      return false;
	    } 
	    this.setState({
	      valid: {
	        message: true,
	      }
	    });
	    return true;
	  };

  handleChange = (e, { name, value }) => this.setState({commentsForm: { ...this.state.commentsForm, [name]: value }});
  handleOpen = () => this.setState({ modalOpen: true });
  handleClose = () => this.setState({ modalOpen: false });
  
  handleCancel = () => {
	  this.setState(this.getInitState());
	  this.handleClose();
  };

  getModalTrigger = () => {
    if (this.notesExist()) {
      return <Popup trigger={<Icon link name={this.getIconName()}
                                   onClick={this.handleOpen}/> }
              content={this.state.commentsForm.notes} />;
    }
    return <Icon link name={this.getIconName()}
                 onClick={this.handleOpen}/>;
  };

  render() {
    const {rowId} = this.props;
    return (
      <Modal trigger={this.getModalTrigger()} size='small' open={this.state.modalOpen}>
      <Modal.Header>Add Notes</Modal.Header>
      <Modal.Content>
           
    <Form>
    <Form.Field>
      <TextArea autoHeight value={this.state.commentsForm.notes} name="notes" maxlength="150" style={{width: '100%'}} placeholder='Add Notes...' onChange={this.handleChange} />
      {!this.state.valid.message ? <Label basic color='red' pointing>Please enter a value</Label> : null}
      
      </Form.Field>
      </Form>
      </Modal.Content>
      <Modal.Actions>
        <Button onClick={this.handleCancel}>
           Cancel
        </Button>
        <Button primary onClick={()=>this.addComment(rowId)}>
          <Icon name='checkmark' /> Add
        </Button>
    </Modal.Actions>
    </Modal>
    );
  }
}

export default CommentModal;
