import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Menu, Container, Dropdown} from 'semantic-ui-react';
import {isEmpty} from 'ramda';
import Utils from '../utils/Utils';

class Navigation extends Component {
    static propTypes = {
        user: PropTypes.objectOf(PropTypes.string).isRequired
    };

    getUserName = (user) => {
        if (user && !isEmpty(user)) {
            return `${user.firstName} ${user.lastName} (${user.username})`;
        }
    };
    render() {
        const {user} = this.props;
        return (
            <Menu color='blue' inverted fixed='top'>
                <Menu.Item header>UXID Data Cleanup Tool</Menu.Item>
                <Menu.Menu position='right'>
                    <Dropdown icon='user' pointing className='link item'>
                        {user &&!isEmpty(user) ? <Dropdown.Menu>
                            <Dropdown.Item>{this.getUserName(user)}</Dropdown.Item>
                            <Dropdown.Divider />
                            { Utils.isAdmin(user) ? <Dropdown.Item href='/addUser'>Add User</Dropdown.Item> : null }
                            <Dropdown.Item href='/logout' >Logout</Dropdown.Item>
                        </Dropdown.Menu> : null}
                    </Dropdown>
                </Menu.Menu>
            </Menu>
        )
        }
    }

export default Navigation;
