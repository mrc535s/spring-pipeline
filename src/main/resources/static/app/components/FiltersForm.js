import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Form } from 'semantic-ui-react';
import CleanupActions from '../actions/CleanupActions';
import FormConstants from '../constants/FormConstants';

class FiltersForm extends Component {

  static propTypes = {
    filters: PropTypes.objectOf(PropTypes.string).isRequired
  };

  state = {
    loading: false
  };

  handleChange = (e, { name, value }) => 
    CleanupActions.updateFilters({ [name]: value })
  

  filter = () => {
    const DEFAULT_PAGE = 1;
    CleanupActions.pageChange(DEFAULT_PAGE);
  };

  clear = () => {
    CleanupActions.clearFilters();
  };

  render() {
    const {
      uxidentity,
      currentBucket,
      sourceSystem,
      idType,
      managementArea,
      workOrderDateRange,
      id,
      netIQStatus,
      netIQJobType,
      netIQDepartment
    } = this.props.filters;
    return (
      <Form loading={this.state.loading}>
        <Form.Group widths='equal'>
          <Form.Select fluid value={currentBucket} label='Current Bucket' name='currentBucket' options={FormConstants.currentBucketOptions} placeholder='Select a Current Bucket' onChange={this.handleChange}/>  
          <Form.Select fluid value={managementArea} label='Management Area' name='managementArea' options={FormConstants.maOptions} placeholder='Select a Management Area' onChange={this.handleChange}/>
          <Form.Select fluid value={sourceSystem} label='Source System' name='sourceSystem' options={FormConstants.sourceSystemOptions} placeholder='Select a Source System' onChange={this.handleChange}/>
          <Form.Select fluid value={idType} label='ID Type' name='idType' options={FormConstants.idTypeOptions} placeholder='Select an Id Type' onChange={this.handleChange}/>
          <Form.Input fluid value={netIQJobType} label='NetIQ Job Type' name='netIQJobType' placeholder='Enter a NetIQ Job Type' onChange={this.handleChange} />
        </Form.Group>
        <Form.Group widths='equal'>
          <Form.Select fluid value={workOrderDateRange} label='Work Order Date Range' name='workOrderDateRange' options={FormConstants.dateRangeOptions} placeholder='Select a Work Order Date Range' onChange={this.handleChange}/>
          <Form.Input fluid value={id} label='Sales/Tech/Op ID' name='id' placeholder='Enter a Sales/Tech/Op ID' onChange={this.handleChange} />
          <Form.Input fluid value={netIQStatus} label='NetIQ Status' name='netIQStatus' placeholder='Enter a NetIQ Status' onChange={this.handleChange} />
          <Form.Input fluid value={netIQDepartment} label='NetIQ Department' name='netIQDepartment' placeholder='Enter a NetIQ Department' onChange={this.handleChange} />
          <Form.Select fluid value = {uxidentity} label='UXID Entity Type' name = 'uxidentity' options={FormConstants.uxidEntityTypeOptions} placeholder='Select a UXID Entity Type' onChange={this.handleChange} />
        </Form.Group>
        <Form.Group>
        <Form.Button onClick={this.filter} primary>Filter</Form.Button>
        <Form.Button onClick={this.clear}>Clear</Form.Button>
        </Form.Group>
      </Form>
    );
  }
}

export default FiltersForm;
