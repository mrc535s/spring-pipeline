import React from "react";
import { render } from "react-dom";
import { BrowserRouter as Router, Route } from "react-router-dom";
import 'semantic-ui-css/semantic.min.css';
import MainContainer from './components/MainContainer';
import InitializeActions from './actions/InitializeActions';

InitializeActions.initApp();

render(
    <Router>
      <div>
        <Route exact path="/" component={MainContainer} />
      </div>
    </Router>,
  document.getElementById("root")
);
