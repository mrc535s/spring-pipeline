package com.charter.datacleanuptool.service;

import com.charter.datacleanuptool.domain.User;

public interface UserService {
    public User findUserByUsername(String username);
    public void saveUser(User user);
}
