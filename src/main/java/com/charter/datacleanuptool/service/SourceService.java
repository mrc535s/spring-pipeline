/**
 * 
 */
package com.charter.datacleanuptool.service;

import java.time.Duration;
import java.time.Instant;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;


import com.charter.datacleanuptool.domain.SourceData;
import com.charter.datacleanuptool.domain.SourceDataFilter;
import com.charter.datacleanuptool.domain.SourceDataSpecification;
import com.charter.datacleanuptool.repository.SourceRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 * @author P2757584
 * This is where the majority of the logic will live for source data.
 * I setup a simple example with the first 10 items.  This should obviously be using
 * parameters sent in from the UI.
 *
 */
@Service
public class SourceService {
	

	private static final Logger logger = LoggerFactory.getLogger(SourceService.class);
	  
	@Autowired
	SourceRepository repository;

	@SuppressWarnings("unchecked")
	public Page <SourceData> getSourceBySpecs(SourceDataFilter myFilter) {
	    SourceDataSpecification spec = new SourceDataSpecification(myFilter);
		Pageable returnPageRequestRows = new PageRequest(myFilter.getPageNumber() - 1, myFilter.getPageSize());
		String SpecValues = spec.toString();
		logger.info("Search filter: {}", myFilter.toString());
		logger.info("Search filter: {}", spec.toString());
		
		Instant start = Instant.now();
		
		Page<SourceData> results = repository.findAll(spec, returnPageRequestRows); // The findAll is defined in the Repository
		
	    Instant end = Instant.now();
	    Duration duration = Duration.between(start, end);
	    logger.info("Retrieved search options in {}.{} seconds", duration.getSeconds(),duration.getNano());
		
	    
		return results;
	}
}
