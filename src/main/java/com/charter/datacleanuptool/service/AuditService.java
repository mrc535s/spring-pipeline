package com.charter.datacleanuptool.service;
 
import java.util.ArrayList;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.charter.datacleanuptool.domain.AuditData;
import com.charter.datacleanuptool.repository.AuditRepository;

@Service
public class AuditService {
			
	@Autowired
	AuditRepository repository;
	
	@Transactional
	public void addAuditRecord(ArrayList<AuditData>data){
		java.util.Date utilDate = new java.util.Date();
		java.sql.Date date = new java.sql.Date(utilDate.getTime());
		 for (int i = 0; i < data.size(); i++){
			 if (data.get(i).getActionAgreed() != null) {
	             data.get(i).setApprovedDate(date);
			 }
			 repository.save(data.get(i));
		 }
		 
		
}

}
	
	
	 
