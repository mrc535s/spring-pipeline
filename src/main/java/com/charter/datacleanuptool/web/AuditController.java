package com.charter.datacleanuptool.web;

import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.charter.datacleanuptool.domain.AuditData;
import com.charter.datacleanuptool.service.AuditService;

@RestController
@RequestMapping("/audit")
public class AuditController {

		@Autowired
		AuditService auditService;
		
//	    @RequestMapping(method=RequestMethod.GET)
//	    public @ResponseBody List <AuditData> getAuditData(@RequestParam(value="filter", required=false, defaultValue="") String filter) {
//	    return auditService.getAudit();
//	    }
       
	  //insert an audit record  
	  @CrossOrigin	
	  @RequestMapping(method=RequestMethod.POST)
	  ResponseEntity<?> addAuditRecord(@RequestBody ArrayList<AuditData>data) {
		  auditService.addAuditRecord(data);
		  ResponseEntity<String> responseEntity = new ResponseEntity<>("Success",
                  HttpStatus.OK);
		  return responseEntity;
	  }
  
}
