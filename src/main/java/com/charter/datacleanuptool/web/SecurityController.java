package com.charter.datacleanuptool.web;

import com.charter.datacleanuptool.domain.User;
import com.charter.datacleanuptool.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@RestController
@RequestMapping("/security")
public class SecurityController {
    @Autowired
    UserService userService;
    @RequestMapping(method = RequestMethod.GET, value = "/activeUser", produces = "application/json")
    @ResponseBody
    public ResponseEntity<?>currentUserName(Principal principal) {
        ResponseEntity<String> responseEntity = new ResponseEntity<>(principal.getName(),
                HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/activeUserDetails", produces = "application/json")
    @ResponseBody
    public User activeUserDetails(@RequestParam("userName") String userName) {
        return userService.findUserByUsername(userName);
    }
}
