/**
 * 
 */
package com.charter.datacleanuptool.web;

/**
 * @author P2757584
 *
 */
import com.charter.datacleanuptool.domain.User;
import com.charter.datacleanuptool.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

@Controller
public class TemplateController {
  @Autowired
  private UserService userService;

  @RequestMapping(value = {"/", "/{path:[^\\.]*}"})
  public String redirect(Model model) {
    return "index";
  }
  /** Login form. */
  @RequestMapping("/login")
  public String login() {
    return "login";
  }

  @RequestMapping(value="/addUser", method = RequestMethod.GET)
  public ModelAndView registration(){
    ModelAndView modelAndView = new ModelAndView();
    User user = new User();
    modelAndView.addObject("user", user);
    modelAndView.setViewName("addUser");
    return modelAndView;
  }

  @RequestMapping(value = "/addUser", method = RequestMethod.POST)
  public ModelAndView createNewUser(@Valid User user, BindingResult bindingResult) {
    ModelAndView modelAndView = new ModelAndView();
    User userExists = userService.findUserByUsername(user.getUsername());
    if (userExists != null) {
      bindingResult
              .rejectValue("username", "error.user",
                      "There is already a user registered with the username provided");
    }
    if (bindingResult.hasErrors()) {
      modelAndView.setViewName("addUser");
    } else {
      userService.saveUser(user);
      modelAndView.addObject("successMessage", "User has been registered successfully");
      modelAndView.addObject("user", new User());
      modelAndView.setViewName("addUser");

    }
    return modelAndView;
  }
}