/**
 * 
 */
package com.charter.datacleanuptool.web;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.charter.datacleanuptool.domain.SourceData;
import com.charter.datacleanuptool.domain.SourceDataFilter;
import com.charter.datacleanuptool.service.SourceService;

/**
 * @author P2757584
 * 
 * This file is used to define the web endpoints and functions for the source data.
 * Since there is only one endpoint needed for get, this is all we need currently.
 * The method to get Source data will need more parameters for pagination
 * and also need a strategy around how to handle filtering.
 *
 */


@RestController
@RequestMapping("/source")
public class SourceController {
	
	
	private static final Logger logger = LoggerFactory.getLogger(SourceController.class);
	
	@Autowired
	SourceService sourceService;
	
	@CrossOrigin	
	@RequestMapping(method=RequestMethod.POST, value = "/getSourceData", consumes = "application/json", produces = "application/json")
    public Page<SourceData> getSourceData(@RequestBody SourceDataFilter myFilter) {
		logger.info("Class: SourceController, calling getSource");
        return sourceService.getSourceBySpecs(myFilter);
    }
}


