/**
 * 
 */
package com.charter.datacleanuptool.repository;

import java.util.List;
import javax.persistence.*;

import org.jboss.logging.Param;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.QueryByExampleExecutor;
import org.springframework.data.repository.query.*;
import org.springframework.stereotype.Repository;

import com.charter.datacleanuptool.domain.SourceData;
import com.charter.datacleanuptool.domain.SourceDataFilter;

/**
 * @author P2757584
 * This is used to query database using JPA and Hibernate
 * This will be used with the SourceData Persistence library
 */

@Repository
//public interface SourceRepository extends CrudRepository<SourceData, Long>, QueryByExampleExecutor<SourceData>,  JpaSpecificationExecutor{
public interface SourceRepository extends CrudRepository<SourceData, Long>, JpaSpecificationExecutor{
	@Query("FROM SourceData")
	List<SourceData> retrieveSourceData(Pageable pageable, SourceDataFilter myFilter);
	
	@Query("FROM SourceData")
	List<SourceData> findAll(Pageable pageable);
	
}
