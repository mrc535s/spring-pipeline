package com.charter.datacleanuptool.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.charter.datacleanuptool.domain.AuditData;

/**
 * @author P2757584
 * This is used to query database using JPA and Hibernate
 * This will be used with the SourceData Persistence library
 */

@Repository
public interface AuditRepository extends CrudRepository<AuditData, Long> {
	
}




