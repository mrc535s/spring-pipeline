package com.charter.datacleanuptool;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DataCleanupToolApplication {

	public static void main(String[] args) {
		SpringApplication.run(DataCleanupToolApplication.class, args);
	}
}
