/**
 * 
 */
package com.charter.datacleanuptool.domain;

/**
 * @author P2757584
 *
 */
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Embeddable
public class SourceId implements Serializable {
	
	private static final Logger logger = LoggerFactory.getLogger(SourceId.class);
	
	private static final long serialVersionUID = 1L;

	@Column(name="SOURCESYSTEM", nullable=false)
	private String sourceSystem;
	@Column(name="SOURCERECORDKEY", nullable=false)
	private String sourceRecordKey;
	@Column(name="IDTYPE", nullable=false)
	private String idType;

	public SourceId(){};
	
	public SourceId(String sourceSystem, String sourceRecordKey, String idType){
		this.sourceSystem = sourceSystem;
		this.sourceRecordKey = sourceRecordKey;
		this.idType = idType;
	}
	
	public String getsourceSystem() {
		return sourceSystem;
	}

	public void setsourceSystem(String sourceSystem) {
		this.sourceSystem = sourceSystem;
	}
	public String getsourceRecordKey() {
		return sourceRecordKey;
	}

	public void setsourceRecordKey(String sourceRecordKey) {
		this.sourceRecordKey = sourceRecordKey;
	}

	public String getidType() {
		return idType;
	}

	public void setidType(String idType) {
		this.idType = idType;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idType == null) ? 0 : idType.hashCode());
		result = prime * result + ((sourceRecordKey == null) ? 0 : sourceRecordKey.hashCode());
		result = prime * result + ((sourceSystem == null) ? 0 : sourceSystem.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SourceId other = (SourceId) obj;
		if (idType == null) {
			if (other.idType != null)
				return false;
		} else if (!idType.equals(other.idType))
			return false;
		if (sourceRecordKey == null) {
			if (other.sourceRecordKey != null)
				return false;
		} else if (!sourceRecordKey.equals(other.sourceRecordKey))
			return false;
		if (sourceSystem == null) {
			if (other.sourceSystem != null)
				return false;
		} else if (!sourceSystem.equals(other.sourceSystem))
			return false;
		return true;
	}
	

	
}
