/**
 * 
 */
package com.charter.datacleanuptool.domain;


/**
 * @author dcannon
 *
 */
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.IdClass;

@Entity @IdClass(AuditId.class)
@Table(name = "ID_CLEANUP_LOG")

public class AuditData {
	
	private String id;
	@Column(name="sourcesystem")
	@Id
	private String sourceSystem;
	@Column(name="sourcerecordkey")
	@Id
	private String sourceRecordKey;
	@Column(name="idtype")
	@Id
	private String idType;
	@Column(name="siteid")
	private String siteId;
	@Column(name="netiqworkerid")
	private String netIqWorkerId;
	@Column(name="firstname")
	private String firstName;
	@Column(name="lastname")
	private String lastName;
	private String discriminator1;
	private String discriminator2;
	private String discriminator3;
	private java.sql.Date senttoteamdate;
	@Column(name="teamname")
	private String teamName;
	@Column(name="actionagreed")
	private String actionAgreed;
	@Column(name="approvername")
	private String approverName;
	@Column(name="approvercomments")
	private String approverComments;
	@Column(name="approveddate")
	private java.sql.Date approvedDate;
	private String notes;
	@Column(name="actiontaken")
	private String actionTaken;
	@Column(name="actiontakennotes")
	private String actionTakenNotes;
	@Column(name="actiontakendate")
	private java.sql.Date actionTakenDate;
	@Column(name="actiontakenuxid")
	private String actionTakenUxid;
	@Column(name="actiontakenuxidnotes")
	private String actionTakenUxidNotes;
	@Column(name="actiontakenuxiddate")
	private java.sql.Date actionTakenUxidDate;
	private String bucket;
	@Column(name="bucket")
	
	public String getSourceSystem() {
		return sourceSystem;
	}

	public void setSourceSystem(String sourceSystem) {
		this.sourceSystem = sourceSystem;
	}

	public String getSourceRecordKey() {
		return sourceRecordKey;
	}

	public void setSourceRecordKey(String sourceRecordKey) {
		this.sourceRecordKey = sourceRecordKey;
	}

	public String getIdType() {
		return idType;
	}

	public void setIdType(String idType) {
		this.idType = idType;
	}

	public String getSiteId() {
		return siteId;
	}

	public void setSiteId(String siteId) {
		this.siteId = siteId;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	
	public String getDiscriminator1() {
		return discriminator1;
	}

	public void setDiscriminator1(String discriminator1) {
		this.discriminator1 = discriminator1;
	}

	public String getDiscriminator2() {
		return discriminator2;
	}

	public void setDiscriminator2(String discriminator2) {
		this.discriminator2 = discriminator2;
	}

	public String getDiscriminator3() {
		return discriminator3;
	}

	public void setDiscriminator3(String discriminator3) {
		this.discriminator3 = discriminator3;
	}


	public java.sql.Date getSenttoteamdate() {
		return senttoteamdate;
	}

	public void setSenttoteamdate(java.sql.Date senttoteamdate) {
		this.senttoteamdate = senttoteamdate;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getNetIqWorkerId() {
		return netIqWorkerId;
	}

	public void setNetIqWorkerId(String netIqWorkerId) {
		this.netIqWorkerId = netIqWorkerId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getTeamName() {
		return teamName;
	}

	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}

	public String getActionAgreed() {
		return actionAgreed;
	}

	public void setActionAgreed(String actionAgreed) {
		this.actionAgreed = actionAgreed;
	}

	public String getApproverName() {
		return approverName;
	}

	public void setApproverName(String approverName) {
		this.approverName = approverName;
	}

	public String getApproverComments() {
		return approverComments;
	}

	public void setApproverComments(String approverComments) {
		this.approverComments = approverComments;
	}

	public java.sql.Date getApprovedDate() {
		return approvedDate;
	}

	public void setApprovedDate(java.sql.Date approvedDate) {
		this.approvedDate = approvedDate;
	}

	public String getActionTaken() {
		return actionTaken;
	}

	public void setActionTaken(String actionTaken) {
		this.actionTaken = actionTaken;
	}

	public String getActionTakenNotes() {
		return actionTakenNotes;
	}

	public void setActionTakenNotes(String actionTakenNotes) {
		this.actionTakenNotes = actionTakenNotes;
	}

	public java.sql.Date getActionTakenDate() {
		return actionTakenDate;
	}

	public void setActionTakenDate(java.sql.Date actionTakenDate) {
		this.actionTakenDate = actionTakenDate;
	}

	public String getActionTakenUxid() {
		return actionTakenUxid;
	}

	public void setActionTakenUxid(String actionTakenUxid) {
		this.actionTakenUxid = actionTakenUxid;
	}

	public String getActionTakenUxidNotes() {
		return actionTakenUxidNotes;
	}

	public void setActionTakenUxidNotes(String actionTakenUxidNotes) {
		this.actionTakenUxidNotes = actionTakenUxidNotes;
	}

	public java.sql.Date getActionTakenUxidDate() {
		return actionTakenUxidDate;
	}

	public void setActionTakenUxidDate(java.sql.Date actionTakenUxidDate) {
		this.actionTakenUxidDate = actionTakenUxidDate;
	}

	public String getBucket() {
		return bucket;
	}

	public void setBucket(String bucket) {
		this.bucket = bucket;
	}
	
	
		
}
