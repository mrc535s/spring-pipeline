package com.charter.datacleanuptool.domain;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

public class SourceDataSpecification implements Specification<SourceData> {
	
	private SourceDataFilter filter;
	
	public SourceDataSpecification(SourceDataFilter myfilter) {
		super();
		this.filter = myfilter;
	}

	@SuppressWarnings("null")
	public Predicate toPredicate(Root<SourceData> root, CriteriaQuery<?> cq,
			CriteriaBuilder cb) {
		
		Predicate p = cb.conjunction();

	    if (filter.getWorkOrderDateRange() != null && !filter.getWorkOrderDateRange().isEmpty()) {
			p.getExpressions().add(cb.equal(root.get("workOrderDateRange"), filter.getWorkOrderDateRange()));
	    }
	    
	    if (filter.getEquipmentDateRange() != null && !filter.getEquipmentDateRange().isEmpty()) {
			p.getExpressions().add(cb.equal(root.get("equipmentDateRange"), filter.getEquipmentDateRange()));
	    }

	    if (filter.getSourceSystem() != null && !filter.getSourceSystem().isEmpty()) {
			p.getExpressions().add(cb.like(root.get("sourceId").get("sourceSystem"), cb.concat(filter.getSourceSystem(), cb.literal("%")) ));
	    }

	    if (filter.getIdType() != null && !filter.getIdType().isEmpty()) {
			p.getExpressions().add(cb.equal(root.get("sourceId").get("idType"), filter.getIdType()));
	    }

	    if (filter.getManagementArea() != null && !filter.getManagementArea().isEmpty()) {
			p.getExpressions().add(cb.equal(root.get("managementArea"), filter.getManagementArea()));
	    }
	    
	    if (filter.getNetIQStatus() != null && !filter.getNetIQStatus().isEmpty()) {
			p.getExpressions().add(cb.equal(root.get("netIqStatus"), filter.getNetIQStatus()));
	    }
	    
	    if (filter.getNetIQDepartment() != null && !filter.getNetIQDepartment().isEmpty()) {
			p.getExpressions().add(cb.equal(root.get("netIqDepartment"), filter.getNetIQDepartment()));
	    }
	    
	    if (filter.getNetIQJobType() != null && !filter.getNetIQJobType().isEmpty()) {
			p.getExpressions().add(cb.equal(root.get("netIqJobType"), filter.getNetIQJobType()));
	    }
	    
	    if (filter.getuxidentity() != null && !filter.getuxidentity().isEmpty()) {
			p.getExpressions().add(cb.equal(root.get("uxidentity"), filter.getuxidentity()));
	    }

		if (filter.getId() != null && !filter.getId().isEmpty()) {
			p.getExpressions().add(cb.equal(root.get("id"), filter.getId()));
		}
		
		if (filter.getCurrentBucket() != null && !filter.getCurrentBucket().isEmpty()) {
			p.getExpressions().add(cb.equal(root.get("currentBucket"), filter.getCurrentBucket()));
		} else {
			p.getExpressions().add(cb.notEqual(root.get("currentBucket"), "Clean"));
		}
		return p;
		
	}

	
}