package com.charter.datacleanuptool.domain;


public class SourceDataFilter {
	private int 	pageNumber;
	private int 	pageSize;
	
	private String 	currentBucket="";
	private String 	workOrderDateRange="";
	private String 	equipmentDateRange="";
	private String 	sourceSystem="";
	private String 	idType="";
	private String 	managementArea="";
	private String 	netIQStatus="";
	private String 	netIQDepartment="";
	private String 	netIQJobType = "";
	private String 	uxidentity = "";
	private String  id = "";

	public int getPageNumber()	{
		return pageNumber;
	};

	public void setPageNumber(int pageNumber) {
		this.pageNumber= pageNumber;
	}

	public int getPageSize()	{
		return pageSize;
	};

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public String getCurrentBucket()	{
		return currentBucket;
	};

	public void setCurrentBucket(String currentBucket)	{
		this.currentBucket = currentBucket;
	};

	public String getWorkOrderDateRange()	{
		return workOrderDateRange;
	};
	
	public void setWorkOrderDateRange(String workOrderDateRange)	{
		this.workOrderDateRange = workOrderDateRange;
	};
	
	public String getEquipmentDateRange()	{
		return equipmentDateRange;
	};
	
	public void setEquipmentDateRange(String equipmentDateRange)	{
		this.equipmentDateRange = equipmentDateRange;
	};
	
	public String getSourceSystem()	{
		return sourceSystem;
	};
	
	public void setSourceSystem(String sourceSystem)	{
		this.sourceSystem = sourceSystem;
	};
	
	public String getIdType()	{
		return idType;
	};
	
	public void setIdType(String idType)	{
		this.idType = idType;
	};
	
	public String getManagementArea()	{
		return managementArea;
	};
	
	public void setManagementArea(String managementArea)	{
		this.managementArea = managementArea;
	};
	
	public String getNetIQStatus()	{
		return netIQStatus;
	};
	
	public void setNetIQStatus(String netIQStatus)	{
		this.netIQStatus = netIQStatus;
	};
	
	public String getNetIQDepartment()	{
		return netIQDepartment;
	};
		
	public void setNetIQDepartment(String netIQDepartment)	{
		this.netIQDepartment = netIQDepartment;
	};
		
	public String getNetIQJobType()	{
		return netIQJobType;
	};
		
	public void setNetIQJobType(String netIQJobType)	{
		this.netIQJobType = netIQJobType;
	};
		
	public String getuxidentity()	{
		return uxidentity;
	};
	
	public void setuxidentity(String uxidentity)	{
		this.uxidentity = uxidentity;
	};

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String toString() {
		StringBuilder filtersettings = new StringBuilder();
		filtersettings.append("Page Number: ").append(getPageNumber()).append("   ");
		filtersettings.append("Page Size  : ").append(getPageSize()).append("   ");
		filtersettings.append("");

		if (currentBucket.trim().length() > 0) {
			filtersettings.append("currentBucket: ").append(getCurrentBucket()).append("   ");
		}

		if (workOrderDateRange.trim().length() > 0) {
			filtersettings.append("workOrderDateRange: ").append(getWorkOrderDateRange()).append("   ");
		}

		if (equipmentDateRange.trim().length() > 0) {
			filtersettings.append("equipmentDateRange: ").append(getEquipmentDateRange()).append("   ");
		}

		if (sourceSystem.trim().length() > 0) {
			filtersettings.append("sourceSystem: ").append(getSourceSystem()).append("   ");
		}

		if (idType.trim().length() > 0) {
			filtersettings.append("idType: ").append(getIdType()).append("   ");
		}

		if (managementArea.trim().length() > 0) {
			filtersettings.append("managementArea: ").append(getManagementArea()).append("   ");
		}

		if (netIQStatus.trim().length() > 0) {
			filtersettings.append("netIQStatus: ").append(getNetIQStatus()).append("   ");
		}

		if (netIQDepartment.trim().length() > 0) {
			filtersettings.append("netIQDepartment: ").append(getNetIQDepartment()).append("   ");
		}

		if (netIQJobType.trim().length() > 0) {
			filtersettings.append("netIQJobType: ").append(getNetIQJobType()).append("   ");
		}

		if (uxidentity.trim().length() > 0) {
			filtersettings.append("uxidentity: ").append(getuxidentity()).append("   ");
		}
		if (id.trim().length() > 0) {
			filtersettings.append("id: ").append(getId()).append("   ");
		}
		return filtersettings.toString();
	}
	 
	 
	public SourceDataFilter() {
		super();
	}

		
	public SourceDataFilter(int pageNumber, int pageSize, String currentBucket, String workOrderDateRange,
		String equipmentDateRange, String sourceSystem, String idType, String managementArea, String netIQStatus,
		String netIQDepartment, String netIQJobType, String uXIdentity, String id) {
	super();
	this.pageNumber = pageNumber;
	this.pageSize = pageSize;
	this.currentBucket = currentBucket;
	this.workOrderDateRange = workOrderDateRange;
	this.equipmentDateRange = equipmentDateRange;
	this.sourceSystem = sourceSystem;
	this.idType = idType;
	this.managementArea = managementArea;
	this.netIQStatus = netIQStatus;
	this.netIQDepartment = netIQDepartment;
	this.netIQJobType = netIQJobType;
	this.uxidentity = uXIdentity;
	this.id = id;
}

}