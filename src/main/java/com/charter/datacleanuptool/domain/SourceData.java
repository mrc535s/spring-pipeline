/**
 * 
 */
package com.charter.datacleanuptool.domain;

/**
 * @author P2757584
 * 
 * Add any columns that are required to send back as part of the request.
 * This is using a sample table as an example.  
 * This will be replaced with the source table information and columns once available
 *
 */

import javax.persistence.Entity;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Table;

import java.sql.Date;

@Entity
@Table(name = "id_cleanup_vw")

public class SourceData {

	@EmbeddedId
	private SourceId sourceId;
	private String id;
    
    private String bucket;
    @Column(name="ACTIONAGREED")
    private String actionAgreed;
	@Column(name="approvername")
	private String approverName;
	@Column(name="approvercomments")
	private String approverComments;

	@Column(name="approveddate")
	private java.sql.Date approvedDate;
	private String notes;
	@Column(name="actiontaken")
	private String actionTaken;
	@Column(name="actiontakennotes")
	private String actionTakenNotes;

	@Column(name="teamname")
	private String teamName;

	@Column(name = "CURRENTBUCKET")
	private String 	currentBucket;
	
	@Column(name = "POTENTIALBUCKET")
	private String 	potentialBucket;

	@Column(name = "DUPECOUNT")
	private Integer 	dupeCount;
	
	@Column(name = "DUPE")
	private String 	dupe;
	
	@Column(name = "MULTICOUNT")
	private Integer 	multiCount;
	
	@Column(name = "MULTI")
	private String 	multi;
	
	@Column(name = "WORKORDERDATE")
	private Date 	workOrderDate;
	
	@Column(name = "WORKORDERDATERANGE")
	private String 	workOrderDateRange;
	
	@Column(name = "EQUIPMENTDATE")
	private Date 	equipmentDate;
	
	@Column(name = "EQUIPMENTDATERANGE")
	private String 	equipmentDateRange;
	
	@Column(name = "JOBDATE")
	private Date 	jobDate;
	
	@Column(name = "JOBDATERANGE")
	private String 	jobDateRange;
	
	@Column(name = "SITEID")
	private String 	siteId;
	
	@Column(name = "NETIQWORKERID")
	private String 	netIqWorkerId;
	
	@Column(name = "FIRSTNAME")
	private String 	firstName;
		
	@Column(name = "LASTNAME")
	private String 	lastName;
	
	@Column(name = "MANAGEMENTAREA")
	private String 	managementArea;
	
	@Column(name = "WORKASSURELOGINID")
	private String 	workAssureLoginId;
	
	@Column(name = "WORKASSUREFIRSTNAME")
	private String 	workAssureFirstName;
	
	@Column(name = "WORKASSURELASTNAME")
	private String 	workAssureLastName;
	
	@Column(name = "WORKASSUREROLE")
	private String 	workAssureRole;
	
	@Column(name = "NETIQENTITYACCOUNT")
	private String 	netIqEntityAccount;
	
	@Column(name = "NETIQSUPERVISORID")
	private String 	netIqSupervisorId;
	
	@Column(name = "NETIQCOMPANY")
	private String 	netIqCompany;
	
	@Column(name = "NETIQSTATUS")
	private String 	netIqStatus;
	
	@Column(name = "NETIQDEPARTMENT")
	private String 	netIqDepartment;
	
	@Column(name = "NETIQREGION")
	private String 	netIqRegion;
	
	@Column(name = "NETIQJOBTYPE")
	private String 	netIqJobType;
	
	@Column(name = "NETIQFIRSTNAME")
	private String 	netIqFirstName;
	
	@Column(name = "NETIQLASTNAME")
	private String 	netIqLastName;
	
	@Column(name = "NETIQTERMEDDATE")
	private Date 	netIqTermedDate;
	
	@Column(name = "NETIQTERMEDDATERANGE")
	private String 	netIqTermedDateRange;
	
	@Column(name = "NETIQHIREDATE")
	private Date 	netIqHireDate;
	
	@Column(name = "NETIQHIREDATERANGE")
	private String 	netIqHireDateRange;
	
	@Column(name = "UXIDENTITY")
	private String 	uxidentity;
	
	@Column(name = "UXIDENTITYACCOUNT")
	private String 	uxidEntityAccount;
	
	@Column(name = "LOADDATE")
	private Date 	loadDate;

	@Column(name = "WORKORDERNUMBER")
	private String 	workOrderNumber;

	@Column(name = "EQUIPMENTNUMBER")
	private String 	equipmentNumber;

	@Column(name = "JOBNUMBER")
	private String 	jobNumber;

	public SourceId getSourceId() {
        return sourceId;
    }

    public void setSourceId(SourceId id) {
        this.sourceId = id;
    }

	public String getCurrentBucket()	{
		return currentBucket;
	};
	
	public void setCurrentBucket(String currentBucket)	{
		this.currentBucket = currentBucket;
	};
	
	public String getPotentialBucket()	{
		return potentialBucket;
	};

	public void setPotentialBucket(String potentialBucket)	{
		this.potentialBucket = potentialBucket;
	};

	public Integer getDupeCount()	{
		return dupeCount;
	};
	
	public void setDupeCount(Integer dupeCount)	{
		this.dupeCount = dupeCount;
	};
	
	public String getDupe()	{
		return dupe;
	};
	
	public void setDupe(String dupe)	{
		this.dupe = dupe;
	};
	
	public Integer getMultiCount()	{
		return multiCount;
	};
	
	public void setMultiCount(Integer multiCount)	{
		this.multiCount = multiCount;
	};
	
	public String getMulti()	{
		return multi;
	};
	
	public void setMulti(String multi)	{
		this.multi = multi;
	};
	
	public Date getWorkOrderDate()	{
		return workOrderDate;
	};
	
	public void setWorkOrderDate(Date workOrderDate)	{
		this.workOrderDate = workOrderDate;
	};
	
	public String getWorkOrderDateRange()	{
		return workOrderDateRange;
	};
	
	public void setWorkOrderDateRange(String workOrderDateRange)	{
		this.workOrderDateRange = workOrderDateRange;
	};
	
	public Date getEquipmentDate()	{
		return equipmentDate;
	};
	
	public void setEquipmentDate(Date equipmentDate)	{
		this.equipmentDate =  equipmentDate;
	};
	
	public String getEquipmentDateRange()	{
		return equipmentDateRange;
	};
	
	public void setEquipmentDateRange(String equipmentDateRange)	{
		this.equipmentDateRange = equipmentDateRange;
	};
	
	public Date getJobDate()	{
		return jobDate;
	};
	
	public void setJobDate(Date jobDate)	{
		this.jobDate = jobDate;
	};
	
	public String getJobDateRange()	{
		return jobDateRange;
	};
	
	public void setJobDateRange(String jobDateRange)	{
		this.jobDateRange = jobDateRange;
	};
	
	public String getApproverName() {
		return approverName;
	}

	public void setApproverName(String approverName) {
		this.approverName = approverName;
	}

	public String getApproverComments() {
		return approverComments;
	}

	public void setApproverComments(String approverComments) {
		this.approverComments = approverComments;
	}

	public java.sql.Date getApprovedDate() {
		return approvedDate;
	}

	public void setApprovedDate(java.sql.Date approvedDate) {
		this.approvedDate = approvedDate;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getActionTaken() {
		return actionTaken;
	}

	public void setActionTaken(String actionTaken) {
		this.actionTaken = actionTaken;
	}

	public String getActionTakenNotes() {
		return actionTakenNotes;
	}

	public void setActionTakenNotes(String actionTakenNotes) {
		this.actionTakenNotes = actionTakenNotes;
	}
	
	public String getFirstName()	{
		return firstName;
	};
	
	public void setFirstName(String firstName)	{
		this.firstName= firstName;
	};
	
	public String getLastName()	{
		return lastName;
	};
	
	public void setLastName(String lastName)	{
		this.lastName= lastName;
	};
	
	public String getManagementArea()	{
		return managementArea;
	};
	
	public void setManagementArea(String managementArea)	{
		this.managementArea = managementArea;
	};
	
	public String getWorkAssureFirstName()	{
		return workAssureFirstName;
	};
	
	public void setWorkAssureFirstName(String workAssureFirstName)	{
		this.workAssureFirstName = workAssureFirstName;
	};
	
	public String getWorkAssureLastName()	{
		return workAssureLastName;
	};
	
	public void setWorkAssureLastName(String workAssureLastName)	{
		this.workAssureLastName = workAssureLastName;
	};
	
	public String getWorkAssureRole()	{
		return workAssureRole;
	};
	
	public void setWorkAssureRole(String workAssureRole)	{
		this.workAssureRole = workAssureRole;
	};
	
	public Date getLoadDate()	{
		return loadDate;
	};
	
	public void setLoadDate(Date loadDate)	{
		this.loadDate = loadDate;
	};
	
	public String getWorkOrderNumber()	{
		return workOrderNumber;
	};
	
	public void setWorkOrderNumber(String workOrderNumber)	{
		this.workOrderNumber = workOrderNumber;
	};
	
	public String getEquipmentNumber()	{
		return equipmentNumber;
	};
	
	public void setEquipmentNumber(String equipmentNumber)	{
		this.equipmentNumber = equipmentNumber;
	};
	
	public String getJobNumber()	{
		return jobNumber;
	};
	
	public void setJobNumber(String jobNumber)	{
		this.jobNumber = jobNumber;
	};
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

//	public String getIdType() {
//		return idType;
//	}
//
//	public void setIdType(String idType) {
//		this.idType = idType;
//	}
	
	
	public String getTeamName() {
		return teamName;
	}

	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}

	public String getSiteId() {
		return siteId;
	}

	public void setSiteId(String siteId) {
		this.siteId = siteId;
	}

	public String getNetIqWorkerId() {
		return netIqWorkerId;
	}

	public void setNetIqWorkerId(String netIqWorkerId) {
		this.netIqWorkerId = netIqWorkerId;
	}

	public String getWorkAssureLoginId() {
		return workAssureLoginId;
	}

	public void setWorkAssureLoginId(String workAssureLoginId) {
		this.workAssureLoginId = workAssureLoginId;
	}

	public String getNetIqEntityAccount() {
		return netIqEntityAccount;
	}

	public void setNetIqEntityAccount(String netIqEntityAccount) {
		this.netIqEntityAccount = netIqEntityAccount;
	}

	public String getNetIqSupervisorId() {
		return netIqSupervisorId;
	}

	public void setNetIqSupervisorId(String netIqSupervisorId) {
		this.netIqSupervisorId = netIqSupervisorId;
	}

	public String getNetIqCompany() {
		return netIqCompany;
	}

	public void setNetIqCompany(String netIqCompany) {
		this.netIqCompany = netIqCompany;
	}

	public String getNetIqStatus() {
		return netIqStatus;
	}

	public void setNetIqStatus(String netIqStatus) {
		this.netIqStatus = netIqStatus;
	}

	public String getNetIqDepartment() {
		return netIqDepartment;
	}

	public void setNetIqDepartment(String netIqDepartment) {
		this.netIqDepartment = netIqDepartment;
	}

	public String getNetIqRegion() {
		return netIqRegion;
	}

	public void setNetIqRegion(String netIqRegion) {
		this.netIqRegion = netIqRegion;
	}

	public String getNetIqJobType() {
		return netIqJobType;
	}

	public void setNetIqJobType(String netIqJobType) {
		this.netIqJobType = netIqJobType;
	}

	public String getNetIqFirstName() {
		return netIqFirstName;
	}

	public void setNetIqFirstName(String netIqFirstName) {
		this.netIqFirstName = netIqFirstName;
	}

	public String getNetIqLastName() {
		return netIqLastName;
	}

	public void setNetIqLastName(String netIqLastName) {
		this.netIqLastName = netIqLastName;
	}

	public Date getNetIqTermedDate() {
		return netIqTermedDate;
	}

	public void setNetIqTermedDate(Date netIqTermedDate) {
		this.netIqTermedDate = netIqTermedDate;
	}

	public String getNetIqTermedDateRange() {
		return netIqTermedDateRange;
	}

	public void setNetIqTermedDateRange(String netIqTermedDateRange) {
		this.netIqTermedDateRange = netIqTermedDateRange;
	}

	public Date getNetIqHireDate() {
		return netIqHireDate;
	}

	public void setNetIqHireDate(Date netIqHireDate) {
		this.netIqHireDate = netIqHireDate;
	}

	public String getNetIqHireDateRange() {
		return netIqHireDateRange;
	}

	public void setNetIqHireDateRange(String netIqHireDateRange) {
		this.netIqHireDateRange = netIqHireDateRange;
	}

	public String getuxidentity() {
		return uxidentity;
	}

	public void setuxidentity(String uxidentity) {
		this.uxidentity = uxidentity;
	}

	public String getUxidEntityAccount() {
		return uxidEntityAccount;
	}

	public void setUxidEntityAccount(String uxidEntityAccount) {
		this.uxidEntityAccount = uxidEntityAccount;
	}

	public String getActionAgreed() {
		return actionAgreed;
	}

	public void setActionAgreed(String actionAgreed) {
		this.actionAgreed = actionAgreed;
	}

	public String getBucket() {
		return bucket;
	}

	public void setBucket(String bucket) {
		this.bucket = bucket;
	}
}
