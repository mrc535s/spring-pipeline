/**
 * 
 */
package com.charter.datacleanuptool.domain;

/**
 * @author P2757584
 *
 */
import java.io.Serializable;
import java.util.Objects;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AuditId implements Serializable {
	
	private static final Logger logger = LoggerFactory.getLogger(AuditId.class);
	
	private static final long serialVersionUID = 1L;

	private String sourceSystem;
	private String sourceRecordKey;
	private String idType;

	public AuditId(){};
	
	public AuditId(String sourceSystem, String sourceRecordKey, String idType){
		this.sourceSystem = sourceSystem;
		this.sourceRecordKey = sourceRecordKey;
		this.idType = idType;
	}
	
	public String getsourceSystem() {
		return sourceSystem;
	}

	public void setsourceSystem(String sourceSystem) {
		this.sourceSystem = sourceSystem;
	}
	public String getsourceRecordKey() {
		return sourceRecordKey;
	}

	public void setsourceRecordKey(String sourceRecordKey) {
		this.sourceRecordKey = sourceRecordKey;
	}

	public String getidType() {
		return idType;
	}

	public void setidType(String idType) {
		this.idType = idType;
	}
	
	@Override
	public boolean equals(Object o) {
	
	    if (o == this) return true;
	    if (!(o instanceof AuditId)) {
	        return false;
	    }
	    AuditId auditId = (AuditId) o;
		StringBuilder joinValues = new StringBuilder();
		joinValues.append("sourceSystem   : ").append(sourceSystem);
		joinValues.append("idType         : ").append(idType);
		joinValues.append("sourceRecordKey: ").append(sourceRecordKey);
	    AuditId.logger.info("Join Condition values: {}",joinValues.toString());
	    try	
	    {
	    return Objects.equals(sourceSystem, auditId.sourceSystem) &&
	           Objects.equals(idType, auditId.idType) &&
	           Objects.equals(sourceRecordKey, auditId.sourceRecordKey); 
	    }
	    catch (Exception e)
	    {
	    	AuditId.logger.info(e.getMessage().toString());
	    	return false;
	    }
	}
	
	@Override
	public int hashCode() {
		StringBuilder joinValues = new StringBuilder();
		joinValues.append("sourceSystem   : ").append(sourceSystem);
		joinValues.append("idType         : ").append(idType);
		joinValues.append("sourceRecordKey: ").append(sourceRecordKey);
	    AuditId.logger.info("Join Condition values: {}",joinValues.toString());
	    return Objects.hash( sourceSystem, idType, sourceRecordKey);
	}
	
}