#!/bin/sh
HOST=$(hostname)
PROFILE=""

if [ "$1" != "" ]; then
	PROFILE="$1"
elif [ "$HOST" = "vm0dnuxidxa0001" ]; then
	PROFILE="dev"
elif [ "$HOST" = "vm0qnuxidxa0001" ]; then
	PROFILE="qa"
elif [ "$HOST" = "vm0qnuxidxa0002" ]; then
	PROFILE="sit"
elif [ "$HOST" = "vm0unuxidxa0001" ] || [ "$HOST" = "vm0unuxidxa0002" ]; then
	PROFILE="uat"
elif [ "$HOST" = "vm0pnuxidxa0001" ] || [ "$HOST" = "vm0pnuxidxa0002" ]; then
	PROFILE="prod"
elif [ "$HOST" = "vm0rnuxidxa0001" ] || [ "$HOST" = "vm0rnuxidxa0002" ]; then
	PROFILE="dr"
fi

nohup java -jar -Djasypt.encryptor.password=com.charter.uxid.webservices.UXID -Dspring.profiles.active=$PROFILE -Xms1536m -Xmx1536m data-cleanup-tool*.jar >> /apps/uxid/apps/uxid-data-cleanup/logs/nohup-$(date +%Y-%m-%d).out & echo $! > pid
